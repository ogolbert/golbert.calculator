﻿var puffer = 0;
var operand = 0;
var operator;

var txtInput;

var menu; //for change color


function SetEvent() {
    var items = document.getElementsByClassName('cssButton');
    for (var index = 0; index < items.length; index++)
    {
        var btn = items[index];
        btn.addEventListener('click', OnButtonClick, false);
    }
}

function GetTarget(e)
{
    e = e || event;
    return e.target || e.srcElement;
}

function OnButtonClick(e) {
    txtInput = document.getElementById("idInput");

    value = e.currentTarget.textContent;

    if (value == "+" || value == "-" || value == "*" || value == "/") {
        operator = value;
        puffer = operand;
        operand = 0;
        txtInput.value = operator;
    }
    else if (value == "clear") {
        operator = "";
        operand = 0;
        puffer = 0;
        txtInput.value = "";
    }
    else if (value == "=") {
        puffer = DoCalculate();
        txtInput.value = Number(puffer);
        operand = puffer;
    }
    else {
        operand = Number(operand + value);
        txtInput.value = operand;
    }


}

function DoCalculate()
{
    var out = 0;
    switch (operator)
    {
        case "+":
            out = Number(operand) + Number(puffer);
            break;
        case "-":
            out = Number(puffer) - Number(operand);;
            break;
        case "*":
            out = Number(operand!=null ? operand: 0) * Number(puffer);
            break;
        case "/":
            out = Number(puffer) / Number(operand);
            break;
        default:
            out = -999;
            break;
    }
    return out;
}

//#change color
//for static

function _FileRef()
{
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    return fileref;
}


function SetColor(col)
{
    var file;
    var fileref = _FileRef();

    //var fileref = document.createElement("link")
    //fileref.setAttribute("rel", "stylesheet")
    //fileref.setAttribute("type", "text/css")

    if (col == "hot")
        file = "content/StyleSheet.css";
    else
        file = "content/StyleSheetCold.css"
    
    fileref.setAttribute("href", file)
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

//for dynamic from menu
function ChangeColorClick(e) {
    var file;
    var fileref = _FileRef();

    if (e.currentTarget.id == "hot")
        file = "content/StyleSheet.css";
    else
        file = "content/StyleSheetCold.css";

    fileref.setAttribute("href", file);
    document.getElementsByTagName("head")[0].appendChild(fileref);

    if (menu != null) menu.hide();
}

//#menu


function ActivateMenu()
{
    var container = document.getElementById("idInput");
    container.addEventListener('mouseenter', OnMenuCall, false);
}

function OnMenuCall(e)
{
    e = e || window.event;
    menu = new Menu();
    menu.show(e.clientX, e.clientY)
}

function CreateButton(text)
{
    var but = document.createElement("button");
    but.style.width = "180";
    but.style.height = "30";
    but.style.left = "10";
    but.style.top = "10";
    but.textContent = text;
    but.id = text;
    but.addEventListener('click', ChangeColorClick, false);
    return but;
}


function Menu()
{
    this.menu = document.createElement("div");
    this.menu.style.position = "absolute";
    this.menu.className = "menu";
    this.menu.style.width = "200px";
    this.menu.style.height = "200px";
    this.menu.style.backgroundColor = "gray";

    this.menu.appendChild(CreateButton("cold"));
    this.menu.appendChild(CreateButton("hot"));

    Menu.prototype.show = function (x, y) {
        this.menu.style.left = x + "px";
        this.menu.style.top = y + "px";
        this.menu.style.borderColor = "red";
        this.menu.style.borderWidth = "5px";
        this.menu.style.borderStyle = "groove";
        this.menu.style.zIndex = "3";
        this.menu.style.visibility = "visible";
        var winow = document.body;
        if (this.menu.parentNode != winow) {
            winow.appendChild(this.menu);
        }
    }

    Menu.prototype.hide = function ()
    {
        this.menu.style.visibility = "hidden";
    }
}




