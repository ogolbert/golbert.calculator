﻿module('Calculator Test Suite', { setup: function () { initialize(); } })

test("Operand Button Click Test", function () {
    expect(10);
    var btns = document.getElementsByClassName('cssButtonOperand');
    for (var i = 0; i < btns.length; i++) {
        var btn = btns[i];
        QUnit.triggerEvent(btn, "click");
        var result = txtInput.value[txtInput.value.length-1];
        var expected = String(((i == 9 ? -1 : i) + 1));
        equal(result, expected, 'expected value ' + expected + ' actual value ' + result);
    }
})

test("Plus Button Test", function () {
    expect(1);
    puffer = '5';
    operand = '10';
    //txtInput.value;
    var btn = document.getElementsByClassName('cssButtonOperation')[2];
    QUnit.triggerEvent(btn, "click");
    var expected = '15';
    equal(txtInput.value, expected, 'Expected value: ' + expected + ' actual value: ' + txtInput.value);
})